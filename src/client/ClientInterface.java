package client;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Vector;

/**
 * @author Cristian Onciu
 *
 */
public interface ClientInterface extends Remote
	{
		// public String getName() throws RemoteException;
		public void getMessage(String message, String fromUser)
				throws RemoteException;

		public Vector<String> getReccomendations() throws RemoteException;

		public Vector<String> updateUserList() throws RemoteException;
	}
