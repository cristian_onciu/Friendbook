package client;

import java.awt.Color;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Vector;
import server.ServerInterface;

/**
 * @author Cristian Onciu
 *
 */
public class ClientController extends UnicastRemoteObject implements
		ClientInterface
	{
		private static final long serialVersionUID = 1L;
		private ServerInterface server;
		private ClientFrame frame;

		protected ClientController() throws RemoteException
			{
				super();
			}

		public void setFrame(ClientFrame frame)
			{
				this.frame = frame;
			}

		@Override
		public void getMessage(String message, String fromUser)
				throws RemoteException
			{
				String m = fromUser + ": " + message + "\n";
				frame.displayMessasge(m, Color.BLACK);
			}

		@Override
		public Vector<String> getReccomendations() throws RemoteException
			{
				// TODO Auto-generated method stub
				return null;
			}

		public boolean checkUserExists(String serverIP, String userName)
			{
				boolean ok = false;
				String url = "rmi://" + serverIP + "/friendship";
				try
					{
						server = (ServerInterface) Naming.lookup(url);
						Vector<String> us = server.getUserList();
						for (String u : us)
							if (u.equals(userName)) ok = true;
					}
				catch (MalformedURLException e)
					{
						e.printStackTrace();
					}
				catch (RemoteException e)
					{
						e.printStackTrace();
					}
				catch (NotBoundException e)
					{
						e.printStackTrace();
					}
				return ok;
			}

		public void login(String serverIP, String userName)
			{
				try
					{
						String url = "rmi://" + serverIP + "/friendship";
						server = (ServerInterface) Naming.lookup(url);
						server.login(this, userName);
						frame.enableServerIPField(false);
						frame.enableUserNameFiled(false);
					}
				catch (MalformedURLException e)
					{
						e.printStackTrace();
					}
				catch (RemoteException e)
					{
						e.printStackTrace();
					}
				catch (NotBoundException e)
					{
						e.printStackTrace();
					}
			}

		public void logout()
			{
				try
					{
						server.logout(this);
						System.exit(0);
					}
				catch (RemoteException e)
					{
						e.printStackTrace();
					}
			}

		@Override
		public Vector<String> updateUserList()
			{
				Vector<String> userList = new Vector<String>();
				try
					{
						userList = server.getUserList();
						frame.updateUserList(userList);
					}
				catch (RemoteException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				return userList;
			}

		public void sendMessage(String message, String userName,
				String selectedUserFromList)
			{
				if (!userName.equals(selectedUserFromList)) try
					{
						frame.displayMessasge(userName + ":" + message + "\n", Color.BLUE);
						server.sendMessage(message, userName, selectedUserFromList);
					}
				catch (RemoteException e)
					{
						// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
	}
