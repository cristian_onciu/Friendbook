package server;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

/**
 * @author Cristian Onciu
 *
 */
public class ServerFrame extends JFrame implements ServerView
	{
		private JTextArea	area;

		private JPanel createPanel()
			{
				JPanel Server = new JPanel();
				area = new JTextArea(20, 50);
				area.setEditable(false);
				Server.add(new JScrollPane(area));
				return Server;
			}

		private void createGUI()
			{
				//JFrame.setDefaultLookAndFeelDecorated(true);
				JFrame frame = new JFrame("Server");
				// Set the content pane.
				frame.setContentPane(createPanel());
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.pack();
				frame.setVisible(true);
			}

		public ServerFrame()
			{
				createGUI();
			}

		@Override
		public void displayText(final String text)
			{
				SwingUtilities.invokeLater(new Runnable()
					{
						@Override
						public void run()
							{
								area.append(text + "\n");
							}
					});
			}
	}